//
//  TableViewDetailControllerViewController.h
//  
//
//  Created by Salman Jawed on 26/11/2015.
//
//

#import <UIKit/UIKit.h>

@interface TableViewDetailControllerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@property NSString *textToSend;

@end
