//
//  ViewController.m
//  MyProject
//
//  Created by Salman Jawed on 24/11/2015.
//  Copyright (c) 2015 Salman Jawed. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickAction:(UIButton *)sender {
    
    if([[sender currentTitle] isEqualToString:@"Alert"]){
        //alert
        
      
        
        if([[self.textField text] isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"                                                        message:@"No text entered" delegate:nil                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Text entered"                                                        message:[self.textField text]                                                       delegate:nil                                              cancelButtonTitle:@"OK"                                              otherButtonTitles:nil];
            [alert show];
        }


    }else{
        //next screen
        
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        if([[self.textField text] isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"                                                        message:@"No text entered" delegate:nil                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }else{
            DetailViewController *detailView=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailView"];
            
            detailView.textToSend=[self.textField text];
            
            [self.navigationController pushViewController:detailView animated:YES];
            
        }
        
        
    }
    
}
@end
