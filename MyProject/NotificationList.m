//
//  NotificationList.m
//
//  Created by   on 11/12/2015
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "NotificationList.h"


NSString *const kNotificationListReason = @"Reason";
NSString *const kNotificationListIsRead = @"isRead";
NSString *const kNotificationListRecordTitle = @"RecordTitle";
NSString *const kNotificationListRecordStatusName = @"RecordStatusName";
NSString *const kNotificationListId = @"Id";
NSString *const kNotificationListRecordDate = @"RecordDate";
NSString *const kNotificationListRecordStatusId = @"RecordStatusId";
NSString *const kNotificationListModuleType = @"ModuleType";
NSString *const kNotificationListRecordDescription = @"RecordDescription";
NSString *const kNotificationListRecordId = @"RecordId";


@interface NotificationList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NotificationList

@synthesize reason = _reason;
@synthesize isRead = _isRead;
@synthesize recordTitle = _recordTitle;
@synthesize recordStatusName = _recordStatusName;
@synthesize notificationListIdentifier = _notificationListIdentifier;
@synthesize recordDate = _recordDate;
@synthesize recordStatusId = _recordStatusId;
@synthesize moduleType = _moduleType;
@synthesize recordDescription = _recordDescription;
@synthesize recordId = _recordId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.reason = [self objectOrNilForKey:kNotificationListReason fromDictionary:dict];
            self.isRead = [self objectOrNilForKey:kNotificationListIsRead fromDictionary:dict];
            self.recordTitle = [self objectOrNilForKey:kNotificationListRecordTitle fromDictionary:dict];
            self.recordStatusName = [self objectOrNilForKey:kNotificationListRecordStatusName fromDictionary:dict];
            self.notificationListIdentifier = [[self objectOrNilForKey:kNotificationListId fromDictionary:dict] doubleValue];
            self.recordDate = [self objectOrNilForKey:kNotificationListRecordDate fromDictionary:dict];
            self.recordStatusId = [[self objectOrNilForKey:kNotificationListRecordStatusId fromDictionary:dict] doubleValue];
            self.moduleType = [[self objectOrNilForKey:kNotificationListModuleType fromDictionary:dict] doubleValue];
            self.recordDescription = [self objectOrNilForKey:kNotificationListRecordDescription fromDictionary:dict];
            self.recordId = [[self objectOrNilForKey:kNotificationListRecordId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.reason forKey:kNotificationListReason];
    [mutableDict setValue:self.isRead forKey:kNotificationListIsRead];
    [mutableDict setValue:self.recordTitle forKey:kNotificationListRecordTitle];
    [mutableDict setValue:self.recordStatusName forKey:kNotificationListRecordStatusName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.notificationListIdentifier] forKey:kNotificationListId];
    [mutableDict setValue:self.recordDate forKey:kNotificationListRecordDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.recordStatusId] forKey:kNotificationListRecordStatusId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.moduleType] forKey:kNotificationListModuleType];
    [mutableDict setValue:self.recordDescription forKey:kNotificationListRecordDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.recordId] forKey:kNotificationListRecordId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.reason = [aDecoder decodeObjectForKey:kNotificationListReason];
    self.isRead = [aDecoder decodeObjectForKey:kNotificationListIsRead];
    self.recordTitle = [aDecoder decodeObjectForKey:kNotificationListRecordTitle];
    self.recordStatusName = [aDecoder decodeObjectForKey:kNotificationListRecordStatusName];
    self.notificationListIdentifier = [aDecoder decodeDoubleForKey:kNotificationListId];
    self.recordDate = [aDecoder decodeObjectForKey:kNotificationListRecordDate];
    self.recordStatusId = [aDecoder decodeDoubleForKey:kNotificationListRecordStatusId];
    self.moduleType = [aDecoder decodeDoubleForKey:kNotificationListModuleType];
    self.recordDescription = [aDecoder decodeObjectForKey:kNotificationListRecordDescription];
    self.recordId = [aDecoder decodeDoubleForKey:kNotificationListRecordId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_reason forKey:kNotificationListReason];
    [aCoder encodeObject:_isRead forKey:kNotificationListIsRead];
    [aCoder encodeObject:_recordTitle forKey:kNotificationListRecordTitle];
    [aCoder encodeObject:_recordStatusName forKey:kNotificationListRecordStatusName];
    [aCoder encodeDouble:_notificationListIdentifier forKey:kNotificationListId];
    [aCoder encodeObject:_recordDate forKey:kNotificationListRecordDate];
    [aCoder encodeDouble:_recordStatusId forKey:kNotificationListRecordStatusId];
    [aCoder encodeDouble:_moduleType forKey:kNotificationListModuleType];
    [aCoder encodeObject:_recordDescription forKey:kNotificationListRecordDescription];
    [aCoder encodeDouble:_recordId forKey:kNotificationListRecordId];
}

- (id)copyWithZone:(NSZone *)zone
{
    NotificationList *copy = [[NotificationList alloc] init];
    
    if (copy) {

        copy.reason = [self.reason copyWithZone:zone];
        copy.isRead = [self.isRead copyWithZone:zone];
        copy.recordTitle = [self.recordTitle copyWithZone:zone];
        copy.recordStatusName = [self.recordStatusName copyWithZone:zone];
        copy.notificationListIdentifier = self.notificationListIdentifier;
        copy.recordDate = [self.recordDate copyWithZone:zone];
        copy.recordStatusId = self.recordStatusId;
        copy.moduleType = self.moduleType;
        copy.recordDescription = [self.recordDescription copyWithZone:zone];
        copy.recordId = self.recordId;
    }
    
    return copy;
}


@end
