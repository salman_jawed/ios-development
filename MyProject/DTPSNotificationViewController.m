//
//  DTPSNotificationViewController.m
//  MyProject
//
//  Created by Salman Jawed on 11/12/2015.
//  Copyright © 2015 Salman Jawed. All rights reserved.
//

#import "DTPSNotificationViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "NotificationListResponseModel.h"
#import "NotificationList.h"
#import "NotificationListRequestModel.h"

@interface DTPSNotificationViewController ()

@property (strong, nonatomic) NotificationListResponseModel *responseModel;

@end

@implementation DTPSNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title=@"Notifications";
    
    //showing loader
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Step 1 (Create url)
    NSString *string = [NSString stringWithFormat:@"http://appserver1.systemsltd.com/SDTPS.WebAPI/api/Service/"];
    NSURL *url = [NSURL URLWithString:string];

    //Step 2(Create params)
    //using custom params
    NSDictionary *params = @ {@"UserId" :@"343", @"CurrentPage" :@"1",@"CurrentLanguage":@"0",@"Filter":@"" };
    
    //using model
    NotificationListRequestModel *model=[[NotificationListRequestModel alloc] init];
    model.userId=343;
    model.currentPage=1;
    model.currentLanguage=0;
    model.filter=@"";
    NSDictionary *paramsFromModel=[model dictionaryRepresentation];
    
    

    // Step 3(Create manager)
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; //json response
    
    // Step 4(hit service)
    [manager POST:@"GetNotificationsAll"
       parameters:paramsFromModel
       success:^(NSURLSessionDataTask *task, id responseObject) {
              
              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success"
                                                                  message:@"Data fetched"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"Ok"
                                                        otherButtonTitles:nil];
              [alertView show];
              
               [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"success %@",responseObject);
           
           if ([responseObject isKindOfClass:[NSDictionary class]]){
           
               self.responseModel=[[NotificationListResponseModel alloc] initWithDictionary:responseObject];
               NSLog(@"%@",self.responseModel);
               [self.notificationTable reloadData];
               
           }
       }failure:^(NSURLSessionDataTask *task, NSError *error) {
              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
              [alertView show];
        
              NSLog(@"failure %@",error);
               [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



//table view methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
    return [self.responseModel.notificationList count];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//click listener
{
    
 NotificationList *model=self.responseModel.notificationList    [indexPath.row];
    NSString *string=model.recordDescription;
    
    UIAlertView *selectedAlert = [[UIAlertView alloc]
                                  initWithTitle:[NSString stringWithFormat:@"%ld",(long)indexPath.row] message:string delegate:nil cancelButtonTitle:@"Got It" otherButtonTitles:nil];
    [selectedAlert show];
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//getView method
{
    
    static NSString *simpleTableIdentifier = @"cell_notification";
    DTPSNotificationCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    //    if (cell == nil)
    //        {
    //
    //NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
    //
    //cell = [nibArray objectAtIndex:0];
    //
    //}
    
    
    
    
   NotificationList *model=self.responseModel.notificationList    [indexPath.row];
    cell.lblNotification.text = model.recordDescription;
    //    cell.imageView.image = [UIImage imageNamed:[foodImageArray objectAtIndex:indexPath.row]];
    
    //    cell.time.text = [timeArray objectAtIndex:indexPath.row];
    
//    [cell.imageView setImageWithURL:[NSURL URLWithString:@"https://myavantiservices.files.wordpress.com/2015/02/helloworld.gif"]];
    
    return cell;
    
}

@end
