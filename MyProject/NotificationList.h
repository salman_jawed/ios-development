//
//  NotificationList.h
//
//  Created by   on 11/12/2015
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NotificationList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *isRead;
@property (nonatomic, strong) NSString *recordTitle;
@property (nonatomic, assign) id recordStatusName;
@property (nonatomic, assign) double notificationListIdentifier;
@property (nonatomic, strong) NSString *recordDate;
@property (nonatomic, assign) double recordStatusId;
@property (nonatomic, assign) double moduleType;
@property (nonatomic, strong) NSString *recordDescription;
@property (nonatomic, assign) double recordId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
