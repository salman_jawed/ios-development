//
//  AppDelegate.h
//  MyProject
//
//  Created by Salman Jawed on 24/11/2015.
//  Copyright (c) 2015 Salman Jawed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

