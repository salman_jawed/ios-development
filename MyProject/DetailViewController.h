//
//  DetailViewController.h
//  
//
//  Created by Salman Jawed on 24/11/2015.
//
//
#import <UIKit/UIKit.h>


@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *textEnteredLabel;

@property NSString *textToSend;

@end
