//
//  DTPSNotificationViewController.h
//  MyProject
//
//  Created by Salman Jawed on 11/12/2015.
//  Copyright © 2015 Salman Jawed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTPSNotificationCustomCell.h"

@interface DTPSNotificationViewController : UIViewController<UITableViewDataSource,UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *notificationTable;

@end
