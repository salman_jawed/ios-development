//
//  NotificationListRequestModel.h
//
//  Created by   on 11/12/2015
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NotificationListRequestModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double currentPage;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double currentLanguage;
@property (nonatomic, strong) NSString *filter;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
