//
//  CustomCellTableViewController.h
//  
//
//  Created by Salman Jawed on 26/11/2015.
//
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"

@interface CustomCellTableViewController : UIViewController<UITableViewDataSource,UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewObject;

@property(nonatomic,strong) NSMutableArray *tableAray,*foodImageArray,*timeArray;


@end
