//
//  DTPSNotificationCustomCell.h
//  MyProject
//
//  Created by Salman Jawed on 11/12/2015.
//  Copyright © 2015 Salman Jawed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTPSNotificationCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageNotificationType;

@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UIImageView *imageNotificationStatus;

@end
