//
//  main.m
//  MyProject
//
//  Created by Salman Jawed on 24/11/2015.
//  Copyright (c) 2015 Salman Jawed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
