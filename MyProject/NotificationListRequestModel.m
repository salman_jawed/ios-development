//
//  NotificationListRequestModel.m
//
//  Created by   on 11/12/2015
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "NotificationListRequestModel.h"


NSString *const kNotificationListRequestModelCurrentPage = @"CurrentPage";
NSString *const kNotificationListRequestModelUserId = @"UserId";
NSString *const kNotificationListRequestModelCurrentLanguage = @"CurrentLanguage";
NSString *const kNotificationListRequestModelFilter = @"Filter";


@interface NotificationListRequestModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NotificationListRequestModel

@synthesize currentPage = _currentPage;
@synthesize userId = _userId;
@synthesize currentLanguage = _currentLanguage;
@synthesize filter = _filter;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.currentPage = [[self objectOrNilForKey:kNotificationListRequestModelCurrentPage fromDictionary:dict] doubleValue];
            self.userId = [[self objectOrNilForKey:kNotificationListRequestModelUserId fromDictionary:dict] doubleValue];
            self.currentLanguage = [[self objectOrNilForKey:kNotificationListRequestModelCurrentLanguage fromDictionary:dict] doubleValue];
            self.filter = [self objectOrNilForKey:kNotificationListRequestModelFilter fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.currentPage] forKey:kNotificationListRequestModelCurrentPage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kNotificationListRequestModelUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.currentLanguage] forKey:kNotificationListRequestModelCurrentLanguage];
    [mutableDict setValue:self.filter forKey:kNotificationListRequestModelFilter];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.currentPage = [aDecoder decodeDoubleForKey:kNotificationListRequestModelCurrentPage];
    self.userId = [aDecoder decodeDoubleForKey:kNotificationListRequestModelUserId];
    self.currentLanguage = [aDecoder decodeDoubleForKey:kNotificationListRequestModelCurrentLanguage];
    self.filter = [aDecoder decodeObjectForKey:kNotificationListRequestModelFilter];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_currentPage forKey:kNotificationListRequestModelCurrentPage];
    [aCoder encodeDouble:_userId forKey:kNotificationListRequestModelUserId];
    [aCoder encodeDouble:_currentLanguage forKey:kNotificationListRequestModelCurrentLanguage];
    [aCoder encodeObject:_filter forKey:kNotificationListRequestModelFilter];
}

- (id)copyWithZone:(NSZone *)zone
{
    NotificationListRequestModel *copy = [[NotificationListRequestModel alloc] init];
    
    if (copy) {

        copy.currentPage = self.currentPage;
        copy.userId = self.userId;
        copy.currentLanguage = self.currentLanguage;
        copy.filter = [self.filter copyWithZone:zone];
    }
    
    return copy;
}


@end
