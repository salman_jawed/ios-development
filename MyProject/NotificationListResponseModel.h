//
//  NotificationListResponseModel.h
//
//  Created by   on 11/12/2015
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NotificationListResponseModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double total;
@property (nonatomic, assign) id message;
@property (nonatomic, assign) double status;
@property (nonatomic, assign) double errorCode;
@property (nonatomic, strong) NSArray *notificationList;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
