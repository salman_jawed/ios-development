//
//  MyTableViewController.h
//  
//
//  Created by Salman Jawed on 26/11/2015.
//
//

#import <UIKit/UIKit.h>
#import "TableViewDetailControllerViewController.h"

@interface MyTableViewController : UIViewController<UITableViewDataSource,UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewObject;

@property(nonatomic,strong) NSMutableArray *tableData;

@end
