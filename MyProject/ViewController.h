//
//  ViewController.h
//  MyProject
//
//  Created by Salman Jawed on 24/11/2015.
//  Copyright (c) 2015 Salman Jawed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;
- (IBAction)clickAction:(UIButton *)sender;

@end

