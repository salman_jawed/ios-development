//
//  CustomCellTableViewController.m
//  
//
//  Created by Salman Jawed on 26/11/2015.
//
//

#import "CustomCellTableViewController.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@interface CustomCellTableViewController ()

@end

@implementation CustomCellTableViewController

@synthesize tableAray,foodImageArray,timeArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tableAray = [[NSMutableArray alloc] initWithObjects:@"Food 1",@"Food 2",@"Food 3",@"Food 4",@"Food 5",@"Food 6",@"Food 7",@"Food 8",@"Food 9",@"Food 10", nil];
    
        foodImageArray = [[NSMutableArray alloc] initWithObjects:@"image1.png",@"image2.png",@"image3.png",@"image4.png",@"image5.png",@"image6.png",@"image7.png",@"image8.png",@"image9.png",@"image10.png",@"image11.png", nil];
    
    
        timeArray = [[NSMutableArray alloc] initWithObjects:@"30 min",@"22 min",@"5 min",@"44 min",@"22 min",@"33 min",@"50 min",@"25 min",@"12 min",@"10 min", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
return [tableAray count];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//click listener
{
    
UIAlertView *selectedAlert = [[UIAlertView alloc]
initWithTitle:[NSString stringWithFormat:@"%@ Selected", [tableAray objectAtIndex:indexPath.row]] message:[NSString stringWithFormat:@"It takes %@ to prepare!",[timeArray objectAtIndex:indexPath.row]] delegate:nil cancelButtonTitle:@"Got It" otherButtonTitles:nil];
 [selectedAlert show];
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//getView method
{
    
static NSString *simpleTableIdentifier = @"custom_cell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    if (cell == nil)
//        {
//            
//NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
//            
//cell = [nibArray objectAtIndex:0];
//            
//}
    cell.name.text = [tableAray objectAtIndex:indexPath.row];
//    cell.imageView.image = [UIImage imageNamed:[foodImageArray objectAtIndex:indexPath.row]];
    
//    cell.time.text = [timeArray objectAtIndex:indexPath.row];
    
    [cell.imageView setImageWithURL:[NSURL URLWithString:@"https://myavantiservices.files.wordpress.com/2015/02/helloworld.gif"]];

    return cell;
    
}



@end
