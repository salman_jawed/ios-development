//
//  NotificationListResponseModel.m
//
//  Created by   on 11/12/2015
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "NotificationListResponseModel.h"
#import "NotificationList.h"


NSString *const kNotificationListResponseModelTotal = @"Total";
NSString *const kNotificationListResponseModelMessage = @"Message";
NSString *const kNotificationListResponseModelStatus = @"Status";
NSString *const kNotificationListResponseModelErrorCode = @"ErrorCode";
NSString *const kNotificationListResponseModelNotificationList = @"NotificationList";


@interface NotificationListResponseModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NotificationListResponseModel

@synthesize total = _total;
@synthesize message = _message;
@synthesize status = _status;
@synthesize errorCode = _errorCode;
@synthesize notificationList = _notificationList;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.total = [[self objectOrNilForKey:kNotificationListResponseModelTotal fromDictionary:dict] doubleValue];
            self.message = [self objectOrNilForKey:kNotificationListResponseModelMessage fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kNotificationListResponseModelStatus fromDictionary:dict] doubleValue];
            self.errorCode = [[self objectOrNilForKey:kNotificationListResponseModelErrorCode fromDictionary:dict] doubleValue];
    NSObject *receivedNotificationList = [dict objectForKey:kNotificationListResponseModelNotificationList];
    NSMutableArray *parsedNotificationList = [NSMutableArray array];
    if ([receivedNotificationList isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedNotificationList) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedNotificationList addObject:[NotificationList modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedNotificationList isKindOfClass:[NSDictionary class]]) {
       [parsedNotificationList addObject:[NotificationList modelObjectWithDictionary:(NSDictionary *)receivedNotificationList]];
    }

    self.notificationList = [NSArray arrayWithArray:parsedNotificationList];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.total] forKey:kNotificationListResponseModelTotal];
    [mutableDict setValue:self.message forKey:kNotificationListResponseModelMessage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kNotificationListResponseModelStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.errorCode] forKey:kNotificationListResponseModelErrorCode];
    NSMutableArray *tempArrayForNotificationList = [NSMutableArray array];
    for (NSObject *subArrayObject in self.notificationList) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForNotificationList addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForNotificationList addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForNotificationList] forKey:kNotificationListResponseModelNotificationList];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.total = [aDecoder decodeDoubleForKey:kNotificationListResponseModelTotal];
    self.message = [aDecoder decodeObjectForKey:kNotificationListResponseModelMessage];
    self.status = [aDecoder decodeDoubleForKey:kNotificationListResponseModelStatus];
    self.errorCode = [aDecoder decodeDoubleForKey:kNotificationListResponseModelErrorCode];
    self.notificationList = [aDecoder decodeObjectForKey:kNotificationListResponseModelNotificationList];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_total forKey:kNotificationListResponseModelTotal];
    [aCoder encodeObject:_message forKey:kNotificationListResponseModelMessage];
    [aCoder encodeDouble:_status forKey:kNotificationListResponseModelStatus];
    [aCoder encodeDouble:_errorCode forKey:kNotificationListResponseModelErrorCode];
    [aCoder encodeObject:_notificationList forKey:kNotificationListResponseModelNotificationList];
}

- (id)copyWithZone:(NSZone *)zone
{
    NotificationListResponseModel *copy = [[NotificationListResponseModel alloc] init];
    
    if (copy) {

        copy.total = self.total;
        copy.message = [self.message copyWithZone:zone];
        copy.status = self.status;
        copy.errorCode = self.errorCode;
        copy.notificationList = [self.notificationList copyWithZone:zone];
    }
    
    return copy;
}


@end
